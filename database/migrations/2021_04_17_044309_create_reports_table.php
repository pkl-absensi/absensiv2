<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('dateCreated')->nullable();
            $table->datetime('dateUpdated')->nullable();
            $table->datetime('dateDeleted')->nullable();

            $table->bigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null');
            $table->integer('onTime')->nullable();
            $table->integer('late')->nullable();
            $table->integer('permit')->nullable();
            $table->integer('sick')->nullable();
            $table->integer('checkOutEarly')->nullable();
            $table->integer('totalHoursWorked')->nullable();
            $table->integer('totalFurloughs')->nullable();
            $table->date('date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
