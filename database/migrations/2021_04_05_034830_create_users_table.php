<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('address')->nullable();
            $table->date('birthDate')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('position')->nullable();
            $table->date('joinDate')->nullable();
            $table->date('endDate')->nullable();
            $table->string('status')->nullable();
            $table->date('furloughStart')->nullable();
            $table->date('furloughEnd')->nullable();
            $table->integer('furloughRemaining')->nullable();
            $table->string('role');
            $table->datetime('dateCreated')->nullable();
            $table->datetime('dateUpdated')->nullable();
            $table->datetime('dateDeleted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
