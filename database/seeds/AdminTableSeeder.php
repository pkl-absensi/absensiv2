<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'name' => 'Admin RHA',
            'email' => 'admin@gmail.com',
            'password' => app('hash')->make('admin123'),
            'address' => 'malang',
            'birthDate' => '1997-09-07',
            'phoneNumber' => '0852741936',
            'position' => 'DevOps Admin',
            'status' => 'Active',
            'role' => 'admin',
            'joinDate' => '2021-01-01',
        ]);
        //php artisan db:seed --class=AdminTableSeeder
    }
}
