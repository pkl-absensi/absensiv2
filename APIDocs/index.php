<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Absensi.id - API Documentation</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-calendar"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Absensi <sup>API </sup></div>
            </a>

            <!-- Nav Item - Dashboard -->

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Public Service
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#auth" aria-expanded="true" aria-controls="auth">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Authentication</span>
                </a>
                <div id="auth" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="?page=auth#login"><span class="badge bg-warning" style="color:white; font-weight: bold;">POST</span> Login</a>
                        <a class="collapse-item" href="?page=auth#refreshtoken"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Refresh token</a>
                        <a class="collapse-item" href="?page=auth#logout"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Logout</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#user" aria-expanded="true" aria-controls="user">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>User/Employee</span>
                </a>
                <div id="user" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="?page=user#updateUserSelf"><span class="badge bg-primary" style="color:white; font-weight: bold;">PUT</span> Update user self</a>
                        <a class="collapse-item" href="?page=user#getStatus"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Get User with Status</a>
                        <a class="collapse-item" href="?page=user#getAllUser"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Get All User</a>
                        <a class="collapse-item" href="?page=user#getSelf"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Get Self</a>
                        <a class="collapse-item" href="?page=user#getUserById"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Get User By ID</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#attendance" aria-expanded="true" aria-controls="attendance">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Attendance</span>
                </a>
                <div id="attendance" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="?page=attendance#checkin"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Check in</a>
                        <a class="collapse-item" href="?page=attendance#checkout"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Check out</a>
                        <a class="collapse-item" href="?page=attendance#attendancelist"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Attendance list</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#furlough" aria-expanded="true" aria-controls="furlough">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Furlough</span>
                </a>
                <div id="furlough" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="?page=furlough#addFurlough"><span class="badge bg-primary" style="color:white; font-weight: bold;">PUT</span> Add furlough</a>
                        <a class="collapse-item" href="?page=furlough#getFurloughSelf"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Get furlough Self</a>
                        <a class="collapse-item" href="?page=furlough#getFurloughById"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Get furlough By ID</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#report" aria-expanded="true" aria-controls="report">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Report</span>
                </a>
                <div id="report" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="?page=report#reportmonthlyself"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Self report monthly</a>
                        <a class="collapse-item" href="?page=report#reportself"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Self report</a>
                        <a class="collapse-item" href="?page=report#reportmonthlyemployee"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> User report monthly</a>
                        <a class="collapse-item" href="?page=report#reportemployee"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> User report</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                BackOffice Service
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#backoffice" aria-expanded="true" aria-controls="backoffice">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>BackOffice</span>
                </a>
                <div id="backoffice" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="?page=report#reportmonthlyself"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Self report monthly</a>
                        <a class="collapse-item" href="?page=report#reportself"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> Self report</a>
                        <a class="collapse-item" href="?page=report#reportmonthlyemployee"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> User report monthly</a>
                        <a class="collapse-item" href="?page=report#reportemployee"><span class="badge bg-success" style="color:white; font-weight: bold;">GET</span> User report</a>
                    </div>
                </div>
            </li>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <?php
                    if (isset($_GET['page'])) {
                        switch ($_GET['page']) {
                            case 'auth':
                                include("auth.html");
                                break;
                            case 'attendance':
                                include("attendance.html");
                                break;
                            case 'user':
                                include("user.html");
                                break;
                            case 'furlough':
                                include("furlough.html");
                                break;
                            case 'report':
                                include("report.html");
                                break;

                            default:
                                include("auth.html");
                                break;
                        }
                    } else {
                        include("auth.html");
                    }
                    ?>

                </div>
                <!-- End of Main Content -->

            </div>

            <!-- End of Main Content -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>