<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'attendance', 'middleware' => ['authUser']], function () use ($router) {
    $router->get('/checkin', 'Attendance\AttendanceController@checkIn');
    $router->get('/checkoutbreak', 'Attendance\AttendanceController@checkOutBreak');
    $router->get('/checkinbreak', 'Attendance\AttendanceController@checkInBreak');
    $router->get('/checkout', 'Attendance\AttendanceController@checkOut');
    $router->get('/today', 'Attendance\AttendanceController@attendance');
    $router->get('/today/self', 'Attendance\AttendanceController@attendanceSelf');
    $router->get('/{date}', 'Attendance\AttendanceController@attendance');
    $router->get('/{date}/self', 'Attendance\AttendanceController@attendanceSelf');
    // $router->put('/status/{id}', 'Attendance\AttendanceController@editStatus');
});

$router->group(['prefix' => 'report', 'middleware' => ['authUser']], function () use ($router) {
    $router->get('/monthly/employee/', 'Report\ReportController@allEmployeeReportMonthly');
    $router->get('/self', 'Report\ReportController@myReport');
    $router->get('/employee/{id}', 'Report\ReportController@employeeReport');
    $router->get('/{date}/employee/', 'Report\ReportController@allEmployeeReportMonthly');
    $router->get('/monthly/employee/{id}[/{date}]', 'Report\ReportController@employeeReportMonthly');
    $router->get('/monthly/self[/{date}]', 'Report\ReportController@myReportMonthly');
});

$router->group(['prefix' => 'token', 'middleware' => ['cors']], function () use ($router) {
    $router->post('/login', 'Auth\AuthController@selfToken');
    $router->get('/refreshToken', 'Auth\AuthController@refreshToken');
    $router->get('/logoutToken', 'Auth\AuthController@logoutToken');
});


$router->group(['prefix' => 'user', 'middleware' => ['authUser']], function () use ($router) {
    // $router->post('register', 'User\UserController@register');
    $router->put('/update', 'User\UserController@updateSelf');
    $router->put('/retired/{userId}', 'User\UserController@retired');

    $router->get('/furlough', 'User\UserController@getFurloughUser');
    $router->get('/retired', 'User\UserController@getRetired');
    $router->get('/active', 'User\UserController@getActiveUser');
    $router->get('/', 'User\UserController@getAll');
    $router->get('/self', 'User\UserController@getSelf');
    $router->get('/id/{userId}', 'User\UserController@getUserById');
    // $router->put('refresh', 'User\UserController@refresh');
});

$router->group(['prefix' => 'furlough', 'middleware' => ['authUser']], function () use ($router) {
    $router->put('/add', 'Furlough\FurloughController@addFurlough');
    $router->get('/self', 'Furlough\FurloughController@getSelf');
    $router->get('/{user_id}', 'Furlough\FurloughController@getFurloughById');
});

$router->group(['prefix' => 'backOffice', 'middleware' => ['authAdmin']], function () use ($router) {
    $router->post('/register', 'User\UserController@register');
    $router->put('/retired/{userId}', 'User\UserController@retired');
    // $router->get('/alluser', 'User\UserController@getAll');
    $router->put('/refresh', 'User\UserController@refresh');
    $router->put('/status/{id}', 'Attendance\AttendanceController@editStatus');
    $router->put('/update/{userId}', 'User\UserController@update');
});
