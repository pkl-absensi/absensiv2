<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Furlough extends Model
{
    use SoftDeletes;

    protected $table = "furloughs";
    public static $Type = "furloughs";

    const CREATED_AT = 'dateCreated';
    const UPDATED_AT = 'dateUpdated';
    const DELETED_AT = 'dateDeleted';

    protected $fillable = [
        'user_id',
        'furloughStart',
        'furloughEnd',
        'furloughTotal'

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
