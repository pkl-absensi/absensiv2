<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $table = "employees";
    public static $Type = "employees";

    const CREATED_AT = 'dateCreated';
    const UPDATED_AT = 'dateUpdated';
    const DELETED_AT = 'dateDeleted';

    protected $fillable = [
        'name',
        'address',
        'birthDate',
        'phoneNumber',
        'position',
        'joinDate',
        'endDate',
        'furloughStart',
        'furloughEnd'
    ];

    public function attendances()
    {
        return $this->hasMany('App\Models\Attendance');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report');
    }

    public function attendancesByDate($date) 
    {
        return $this->attendances()->where('date', '=', $date)->get();
    }
}
