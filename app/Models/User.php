<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    use SoftDeletes;
    protected $table = "users";
    public static $Type = "users";

    const CREATED_AT = 'dateCreated';
    const UPDATED_AT = 'dateUpdated';
    const DELETED_AT = 'dateDeleted';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function attendances()
    {
        return $this->hasMany('App\Models\Attendance');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report');
    }

    public function attendancesByDate($date)
    {
        return $this->attendances()->where('date', '=', $date)->get();
    }
    protected $fillable = [
        'name', 
        'email',
        'address',
        'birthDate',
        'phoneNumber',
        'position',
        'joinDate',
        'endDate',
        'furloughStart',
        'furloughEnd'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
