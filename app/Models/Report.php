<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;

    protected $table = "reports";
    public static $Type = "reports";

    const CREATED_AT = 'dateCreated';
    const UPDATED_AT = 'dateUpdated';
    const DELETED_AT = 'dateDeleted';

    protected $fillable = [
        'user_id',
        'onTime',
        'late',
        'permit',
        'sick',
        'checkOutEarly',
        'totalHoursWorked',
        'totalFurloughs',
        'date'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
