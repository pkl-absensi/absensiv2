<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    use SoftDeletes;

    protected $table = "attendances";
    public static $Type = "attendances";

    const CREATED_AT = 'dateCreated';
    const UPDATED_AT = 'dateUpdated';
    const DELETED_AT = 'dateDeleted';

    protected $fillable = [
        'user_id',
        'timeIn',
        'timeOut',
        'breakTimeIn',
        'breakTimeOut',
        'date',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
