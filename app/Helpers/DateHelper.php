<?php

namespace App\Helpers;

use DateInterval;
use DatePeriod;
use DateTime;

class DateHelper
{
    protected $holidays;

    public function __construct($local = '')
    {
        if (empty($local)) {
            $r = file_get_contents("https://github.com/guangrei/Json-Indonesia-holidays/raw/master/calendar.json");
            $this->holidays = json_decode($r, true);
        } else {
            $r = file_get_contents($local);
            $this->holidays = json_decode($r, true);
        }
    }

    public function diffDate($startDate, $endDate)
    {
        $start = new DateTime($startDate);
        $end = new DateTime($endDate);
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');

        $interval = $end->diff($start);

        // total days
        $days = $interval->days;

        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

        foreach ($period as $dt) {
            $curr = $dt->format('D');

            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }

            // (optional) for the updated question
            elseif (!empty($this->holidays[$dt->format('Ymd')])) {
                if ($this->holidays[$dt->format('Ymd')]['deskripsi'] != 'Cuti Bersama') {
                    $days--;
                }
            }
        }

        return $days;
    }
}
