<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Employees extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "address" => $this->address,
            "attendances" => ($this->attendances()->exists()) ? $this->attendances : "tidak hadir",
            "datecreated" => $this->dateCreated,
            "dateupdated" => $this->dateUpdate,
        ];
    }
}
