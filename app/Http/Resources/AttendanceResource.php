<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Attendances extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "employee_id" => $this->employee_id,
            "timeIn" => $this->timeIn
        ];
    }
}
