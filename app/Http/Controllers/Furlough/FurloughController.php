<?php

namespace App\Http\Controllers\Furlough;

use App\Http\Controllers\Controller;
use App\Models\Furlough;
use App\Models\User;
use App\Repositories\Furlough\IFurloughRepository;
use App\Transformers\FurloughTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class FurloughController extends Controller
{
    protected $furlough_repo;

    public function __construct(IFurloughRepository $furlough_repo)
    {
        $this->furlough_repo = $furlough_repo;
    }

    public function addFurlough(Request $request)
    {
        $furlough = $this->furlough_repo->addFurlough($request);
        if ($furlough != null) {
            return $this->singleResponse(
                $request,
                $furlough,
                Furlough::$Type,
                new FurloughTransformer
            );
        }
        return $this->emptyResponse('Jatah Cuti Telah Habis');
        // return $furlough;
    }

    public function getSelf(Request $request)
    {
        $furlough = $this->furlough_repo->getSelfFurlough($request);
        if ($furlough != null) {
            return $this->PaginateResponse(
                $request,
                $furlough,
                Furlough::$Type,
                new FurloughTransformer
            );
        }
        return $this->emptyResponse('Data Tidak Ada');
    }

    public function getFurloughById(Request $request, $user_id)
    {
        $furlough = $this->furlough_repo->getFurloughById($request, $user_id);
        if ($furlough != null) {
            return $this->PaginateResponse(
                $request,
                $furlough,
                Furlough::$Type,
                new FurloughTransformer
            );
        }
        return $this->emptyResponse('Data Tidak Ada');
    }

    // public function test(Request $request)
    // {
    //     $furlough = $this->furlough_repo->getSelfFurlough($request);
    // }
}
