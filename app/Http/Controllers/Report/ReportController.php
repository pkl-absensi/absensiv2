<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Repositories\Report\IReportRepository;
use App\Transformers\ReportTransformer;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    protected $report_repo;
    
    public function __construct(IReportRepository $report_repo)
    {
        $this->report_repo = $report_repo;
    }

    public function myReport(Request $request)
    {
        $report = $this->report_repo->getMyReport($request);
        if ($report != null) {
            return $this->singleResponse(
                $request,
                $report,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse('data kosong');
    }

    public function myReportMonthly(Request $request, $date = null)
    {
        $report = $this->report_repo->getMyReportMonthly($request, $date);
        if ($report != null) {
            return $this->singleResponse(
                $request,
                $report,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse("Data kosong");
    }

    public function employeeReport(Request $request, $id)
    {
        $report = $this->report_repo->getReportByEmployeeId($request, $id);
        if ($report != null) {
            return $this->singleResponse(
                $request,
                $report,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse('data kosong');
    }

    public function employeeReportMonthly(Request $request, $id, $date = null)
    {
        $report = $this->report_repo->getReportMonthlyByEmployeeId($request, $id, $date);
        if ($report != null) {
            return $this->singleResponse(
                $request,
                $report,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse("Data kosong");
    }

    public function allEmployeeReportMonthly(Request $request, $date = null)
    {
        $reports = $this->report_repo->getReportMonthlyEmployee($request, $date);
        if(!$reports->isEmpty()) {
            return $this->paginateResponse(
                $request,
                $reports,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse('Data kosong');
    }
}