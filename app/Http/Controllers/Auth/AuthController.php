<?php

namespace App\Http\Controllers\Auth;

use App\Repositories\Auth\IAuthRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPUnit\Util\Json;

class AuthController extends Controller
{
    public function __construct(IAuthRepository $authRepo)
    {
        $this->authRepo = $authRepo;
    }

    public function selfToken(Request $request)
    {
        $token = $this->authRepo->login($request);

        return $this->respondWithToken($token, "New Token");
    }

    public function refreshToken(Request $request)
    {
        $newToken = $this->authRepo->refreshToken($request);

        return $this->respondWithToken($newToken, "Refresh Token");
    }

    public function logoutToken(Request $request)
    {
        $this->authRepo->refreshToken($request);
        $token = "Token Deleted";
        return $token;
    }
}
