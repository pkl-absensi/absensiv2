<?php

namespace App\Http\Controllers\User;

use App\Repositories\User\IUserRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPUnit\Util\Json;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\Paginator;
use League\Fractal\Serializer\JsonApiSerializer;
use App\Transformers\UserTransformer;
use App\Models\User;

class UserController extends Controller
{
    protected $userRepo;
    public function __construct(IUserRepository $userRepo)
    {
        $this->authRepo = $userRepo;

        // $this->middleware('auth', ['except' => ['register']]);
    }
    public function register(Request $request)
    {
        $user = $this->authRepo->registerUser($request);

        return $this->crateUpdateResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer(),
            null,
            false
        );
    }

    public function update(Request $request, $userId)
    {
        $user = $this->authRepo->updateUser($request, $userId);
        if ($user == null) {
            return $this->noContentResponse();
        }

        return $this->crateUpdateResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer(),
            null,
            false
        );
        // return $user;
    }
    public function updateSelf(Request $request)
    {
        $user = $this->authRepo->updateUserSelf($request);
        if ($user == null) {
            return $this->noContentResponse();
        }

        return $this->crateUpdateResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer(),
            null,
            false
        );
        // return $user;
    }
    public function retired(Request $request, $userId)
    {
        $user = $this->authRepo->updateRetiredUser($request, $userId);

        return $this->crateUpdateResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer(),
            null,
            false
        );
    }
    ######################################

    public function getRetired(Request $request)
    {
        $user = $this->authRepo->getRetiredUser($request);
        return $this->paginateResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer
        );
    }
    public function getActiveUser(Request $request)
    {
        $user = $this->authRepo->getActiveUser($request);
        return $this->paginateResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer
        );
    }
    public function getFurloughUser(Request $request)
    {
        $user = $this->authRepo->getFurloughUser($request);

        if ($user != null) {
            return $this->paginateResponse(
                $request,
                $user,
                User::$Type,
                new UserTransformer
            );
        }

        return $this->emptyResponse("Data kosong");
    }
    public function getAll(Request $request)
    {
        $user = $this->authRepo->getAll($request);
        // return $user;
        return $this->paginateResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer
        );
    }
    public function getUserById(Request $request, $userId)
    {
        $user = $this->authRepo->getUserById($request, $userId);

        return $this->singleResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer
        );
    }

    public function getSelf(Request $request)
    {
        $user = $this->authRepo->getSelf($request);

        return $this->singleResponse(
            $request,
            $user,
            User::$Type,
            new UserTransformer
        );
    }

    public function refresh(Request $request)
    {
        $user = $this->authRepo->refreshStatus($request);
        return $this->emptyResponse("Berhasil Refresh");
    }
}
