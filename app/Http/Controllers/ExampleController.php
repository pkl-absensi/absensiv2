<?php

namespace App\Http\Controllers;

use App\Constants\PaginatorConst;
use App\Http\Resources\Attendances;
use App\Http\Resources\Employees;
use App\Models\Attendance;
use App\Models\Employee;
use App\Models\Report;
use App\Repositories\Attendance\IAttendanceRepository;
use App\Transformers\AttendanceTransformer;
use App\Transformers\EmployeeTransformer;
use Illuminate\Http\Request;
use App\Repositories\Report\IReportRepository;
use App\Transformers\ReportTransformer;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $attendance_repo;
    protected $report_repo;


    public function __construct(IAttendanceRepository $attendance_repo, IReportRepository $report_repo)
    {
        $this->attendance_repo = $attendance_repo;
        $this->report_repo = $report_repo;
    }

    public function test(Request $request, $id, $date = null)
    {
        $report = $this->report_repo->getReportMonthlyByEmployeeId($request, $id, $date);
        if ($report != null) {
            return $this->singleResponse(
                $request,
                $report,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse("Data kosong");
    }

    public function checkIn(Request $request)
    {
        $attendance = $this->attendance_repo->checkInPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-in invalid');
    }

    public function checkOutBreak(Request $request)
    {
        $attendance = $this->attendance_repo->checkOutForBreakPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-out istirahat invalid');
    }

    public function checkInBreak(Request $request)
    {
        $attendance = $this->attendance_repo->checkInAfterBreakPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-in istirahat invalid');
    }

    public function checkOut(Request $request)
    {
        $attendance = $this->attendance_repo->checkOutPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-out invalid');
    }

    public function myReport(Request $request)
    {
        $report = $this->report_repo->getMyReport($request);
        if ($report != null) {
            return $this->singleResponse(
                $request,
                $report,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse('data kosong');
    }

    public function employeeReport(Request $request, $id)
    {
        $report = $this->report_repo->getReportByEmployeeId($request, $id);
        if ($report != null) {
            return $this->singleResponse(
                $request,
                $report,
                Report::$Type,
                new ReportTransformer
            );
        }
        return $this->emptyResponse('data kosong');
    }

    public function attToday(Request $request)
    {
        $attendance = $this->attendance_repo->attendance($request);
        if ($attendance != null) {
            return $this->paginateResponse(
                $request,
                $attendance,
                Employee::$Type,
                new EmployeeTransformer(true)
            );
        }
        return $this->emptyResponse('data kosong');
    }

    public function attByDate(Request $request, $date)
    {
        $attendance = $this->attendance_repo->attendance($request);
        if ($attendance != null) {
            return $this->paginateResponse(
                $request,
                $attendance,
                Employee::$Type,
                new EmployeeTransformer(true, $date)
            );
        }
        return $this->emptyResponse('data kosong');
    }
    //
}
