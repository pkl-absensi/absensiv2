<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Utils\FractalTrait;

class Controller extends BaseController
{
    use FractalTrait;
    protected function respondWithToken($token, $type)
    {
        return response()->json([
            'type' => $type,
            'id' => isset(Auth::user()->id) ? Auth::user()->id : null,
            'role' => isset(Auth::user()->role) ? Auth::user()->role : null,
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ], 200);
    }

    //


}
