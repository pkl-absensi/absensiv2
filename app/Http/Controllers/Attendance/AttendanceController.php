<?php

namespace App\Http\Controllers\Attendance;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use App\Models\User;
use App\Repositories\Attendance\IAttendanceRepository;
use App\Transformers\AttendanceTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    protected $attendance_repo;

    public function __construct(IAttendanceRepository $attendance_repo)
    {
        $this->attendance_repo = $attendance_repo;
    }

    public function checkIn(Request $request)
    {
        $attendance = $this->attendance_repo->checkInPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-in invalid');
    }

    public function checkOutBreak(Request $request)
    {
        $attendance = $this->attendance_repo->checkOutForBreakPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-out istirahat invalid');
    }

    public function checkInBreak(Request $request)
    {
        $attendance = $this->attendance_repo->checkInAfterBreakPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-in istirahat invalid');
    }

    public function checkOut(Request $request)
    {
        $attendance = $this->attendance_repo->checkOutPresence($request);
        if ($attendance != null) {
            return $this->singleResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('check-out invalid');
    }

    public function attendance(Request $request, $date = 'today')
    {
        $attendance = $this->attendance_repo->attendance($request);
        if (!$attendance->isEmpty()) {
            return $this->paginateResponse(
                $request,
                $attendance,
                User::$Type,
                new UserTransformer(true, $date)
            );
        }
        return $this->emptyResponse('Data kosong');
    }

    public function attendanceSelf(Request $request, $date = 'today')
    {
        $attendance = $this->attendance_repo->attendanceSelf($request);
        if ($attendance) {
            return $this->singleResponse(
                $request,
                $attendance,
                User::$Type,
                new UserTransformer(true, $date)
            );
        }
        return $this->emptyResponse('Data kosong');
    }

    public function editStatus(Request $request, $id)
    {
        $attendance = $this->attendance_repo->editStatusAttendance($request, $id);
        if ($attendance != null) {
            return $this->crateUpdateResponse(
                $request,
                $attendance,
                Attendance::$Type,
                new AttendanceTransformer
            );
        }
        return $this->emptyResponse('Data kosong');
    }
}
