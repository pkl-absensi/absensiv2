<?php

namespace App\Transformers;

use App\Models\Employee;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract
{
    protected $availableIncludes = ["attendance"];
    private $params = [];

    public function __construct($attendance = false, $date = null)
    {
        $this->params['attendance'] = ($attendance == null) ? false : $attendance;
        $this->params['date'] = ($date == null) ? date('Y-m-d') : $date;
    }

    public function transform(Employee $emp)
    {
        if ($this->params['attendance'] == true) {
            return [
                'id' => $emp->id,
                'name' => $emp->name,
                'status' => $emp->status,
                'address' => $emp->address,
                'phoneNumber' => $emp->phoneNumber,
                'attendance' => ($emp->furloughEnd < date('Y-m-d')) ? ((!$emp->attendancesByDate($this->params['date'])->isEmpty()) ? $emp->attendancesByDate($this->params['date']) : "Belum hadir") : "Cuti"
            ];
        }

        return [
            'id' => $emp->id,
            'name' => $emp->name,
            'address' => $emp->address,
            'birtDay' => $emp->birtDate,
            'phoneNumber' => $emp->phoneNumber,
            'position' => $emp->position,
            'joinDate' => $emp->joinDate,
            'endDate' => $emp->endDate,
            'furloughStart' => $emp->furloughStart,
            'furloughEnd' => $emp->furloughEnd,
            'dateCreated'   => $emp->dateCreated,
            'dateUpdated'   => $emp->dateUpdated,
            'dateDeleted'   => $emp->dateDeleted
        ];
    }

    public function includeAttendance(Employee $emp)
    {
        $attendance = $emp->attendances;
        if ($attendance !=  null) {
            return $this->collection($attendance, new AttendanceTransformer, "attendances");
        }
    }
}
