<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = ["attendance"];
    private $params = [];

    public function __construct($attendance = false, $date = null)
    {
        $this->params['attendance'] = ($attendance == null) ? false : $attendance;
        $this->params['date'] = ($date == null) ? date('Y-m-d') : $date;
    }
    public function transform(User $user)
    {
        if ($this->params['attendance'] == true) {
            return [
                'id' => $user->id,
                'name' => $user->name,
                'status' => $user->status,
                'address' => $user->address,
                'phoneNumber' => $user->phoneNumber,
                'attendance' => ($user->furloughStart > date('Y-m-d') || $user->furloughEnd < date('Y-m-d')) ? ((!$user->attendancesByDate($this->params['date'])->isEmpty()) ? $user->attendancesByDate($this->params['date']) : "Belum / Tidak hadir") : "Cuti"
            ];
        }
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'address' => $user->address,
            'birthDay' => $user->birthDate,
            'phoneNumber' => $user->phoneNumber,
            'position' => $user->position,
            'furloughStart' =>$user->furloughStart,
            'furloughEnd' =>$user->furloughEnd,
            'status' => $user->status,
            'joinDate' => $user->joinDate,
            'endDate' => $user->endDate,
            'role' => $user->role,
            'dateCreated'   => $user->dateCreated,
            'dateUpdated'   => $user->dateUpdated,
            'dateDeleted'   => $user->dateDeleted
        ];
    }
    public function includeAttendance(User $emp)
    {
        $attendance = $emp->attendances;
        if ($attendance !=  null) {
            return $this->collection($attendance, new AttendanceTransformer, "attendances");
        }
    }
}
