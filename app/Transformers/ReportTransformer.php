<?php

namespace App\Transformers;

use App\Models\Report;
use League\Fractal\TransformerAbstract;

class ReportTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user'];

    public function transform(Report $r)
    {
        return [
            'id' => ($r->id) ? $r->id : 'null',
            'user_id' => $r->user_id,
            'onTime' => $r->onTime,
            'late' => $r->late,
            'notPresent' => $r->notPresent,
            'permit' => $r->permit,
            'sick' => $r->sick,
            'checkOutEarly' => $r->checkOutEarly,
            'totalHoursWorked' => $r->totalHoursWorked,
            'totalFurlougs' => $r->totalFurloughs,
            'date' => $r->date,
            'dateCreated'   => $r->dateCreated,
            'dateUpdated'   => $r->dateUpdated,
            'dateDeleted'   => $r->dateDeleted
        ];
    }

    public function includeUser(Report $r)
    {
        $user = $r->user;
        if ($user !=  null) {
            return $this->item($user, new UserTransformer, "users");
        }
    }
}
