<?php

namespace App\Transformers;

use App\Models\Attendance;
use League\Fractal\TransformerAbstract;

class AttendanceTransformer extends TransformerAbstract
{
    protected $availableIncludes = ["user"];

    public function transform(Attendance $att)
    {
        return [
            'id' => $att->id,
            'user_id' => $att->user_id,
            'timeIn' => $att->timeIn,
            'breakTimeOut' => $att->breakTimeOut,
            'breakTimeIn' => $att->breakTimeIn,
            'timeOut' => $att->timeOut,
            'checkInStatus' => $att->checkInStatus,
            'checkOutStatus' => $att->checkOutStatus,
            'dateCreated'   => $att->dateCreated,
            'dateUpdated'   => $att->dateUpdated,
            'dateDeleted'   => $att->dateDeleted
        ];
    }

    public function includeUser(Attendance $att)
    {
        $user = $att->user;
        if ($user !=  null) {
            return $this->item($user, new UserTransformer, "users");
        }
    }
}
