<?php

namespace App\Transformers;

use App\Models\Furlough;
use League\Fractal\TransformerAbstract;

class FurloughTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user'];

    public function transform(Furlough $f)
    {
        return [
            'id' => ($f->id) ? $f->id : 'null',
            'user_id' => $f->user_id,
            'furloughStart' => $f->furloughStart,
            'furloughEnd' => $f->furloughEnd,
            'furloughTotal' => $f->furloughTotal,
            'dateCreated'   => $f->dateCreated,
            'dateUpdated'   => $f->dateUpdated,
            'dateDeleted'   => $f->dateDeleted
        ];
    }

    public function includeUser(Furlough $f)
    {
        $user = $f->user;
        if ($user !=  null) {
            return $this->item($user, new UserTransformer, "users");
        }
    }
}
