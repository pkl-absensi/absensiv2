<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\AutoPresenceDaily',
        'App\Console\Commands\ReportMonthly'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sg:auto-presence-daily')->dailyAt('21:00');
        $schedule->command('sg:report-monthly')->monthly();
    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
