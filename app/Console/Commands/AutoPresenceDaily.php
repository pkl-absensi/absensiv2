<?php

namespace App\Console\Commands;

use App\Models\Attendance;
use App\Models\User;
use Illuminate\Console\Command;
use Grei\TanggalMerah;

class AutoPresenceDaily extends Command
{
    protected $signature = 'sg:auto-presence-daily';
    protected $description = 'Auto presence daily at 09:00 pm';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $date = new TanggalMerah();
        if(!$date->check()){
            $employees = User::all();
            foreach ($employees as $employee) {
                if (!$employee->attendances()->exists()) {
                    $attendance = new Attendance();
                    $attendance->user_id = $employee->id;
                    $attendance->checkInStatus = ($employee->furloughStart < date('Y-m-d') || $employee->furloughEnd > date('Y-m-d')) ? "Cuti" : "Tidak hadir";
                    $attendance->date = date('Y-m-d');
                    $attendance->save();
                }
            }
        }
    }
}
