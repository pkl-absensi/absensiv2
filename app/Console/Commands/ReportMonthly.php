<?php

namespace App\Console\Commands;

use App\Models\Attendance;
use App\Models\Furlough;
use App\Models\User;
use App\Models\Report;
use DateTime;
use Illuminate\Console\Command;
use Grei\TanggalMerah;


class ReportMonthly extends Command
{
    protected $signature = 'sg:report-monthly';
    protected $description = 'Report users presence monthly';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // $userId = Auth::user()->id;
        $date = new TanggalMerah();
        if (!$date->check()) {
            $employees = User::all();
            foreach ($employees as $employee) {
                $furlough = Furlough::where('user_id', $employee->id)->get();
                $prevMonth = date("m", strtotime('-1 month', strtotime(date('Y-m-d'))));
                $Year = date("Y", strtotime(date('Y-m-d')));
                $attendance = Attendance::where('user_id', $employee->id)->whereMonth('date', $prevMonth)->whereYear('date', $Year)->get();
                $onTime = $attendance->where('checkInStatus', 'Tepat waktu')->count();
                $late = $attendance->where('checkInStatus', 'Telat')->count();
                $notPresent = $attendance->where('checkInStatus', 'Tidak hadir')->count();
                $permit = $attendance->where('checkInStatus', 'Izin')->count();
                $sick = $attendance->where('checkInStatus', 'Sakit')->count();
                $checkOutEarly = $attendance->where('checkInStatus', 'Pulang awal')->count();
                $totalHours = $attendance->sum(function ($t) {
                    $timeIn = DateTime::createFromFormat('Y-m-d H:i:s', ($t->timeIn != null) ? $t->timeIn : date('Y-m-d H:i:s'));
                    $timeOut = DateTime::createFromFormat('Y-m-d H:i:s', ($t->timeOut != null) ? $t->timeOut : date('Y-m-d H:i:s'));
                    // $breakTimeIn = DateTime::createFromFormat('Y-m-d H:i:s', $t->breakTimeIn);
                    // $breakTimeOut = DateTime::createFromFormat('Y-m-d H:i:s', $t->breakTimeOut);
                    $diff = $timeOut->diff($timeIn);
                    return $diff->h;
                });
                $totalFurloughs = $furlough->sum('furloughTotal');
                $report = new Report;
                $report->user_id = $employee->id;
                $report->onTime = $onTime;
                $report->late = $late;
                $report->notPresent = $notPresent;
                $report->permit = $permit;
                $report->sick = $sick;
                $report->checkOutEarly = $checkOutEarly;
                $report->totalHoursWorked = $totalHours;
                $report->totalFurloughs = $totalFurloughs;
                $report->date = date('Y-m-d');
                $report->save();
            }
        }
    }
}
