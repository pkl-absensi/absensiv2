<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Auth\IAuthRepository',
            'App\Repositories\Auth\AuthRepository'
        );
        //
        $this->app->bind(
            'App\Repositories\Attendance\IAttendanceRepository',
            'App\Repositories\Attendance\AttendanceRepository'
        );

        $this->app->bind(
            'App\Repositories\Report\IReportRepository',
            'App\Repositories\Report\ReportRepository'
        );

        $this->app->bind(
            'App\Repositories\User\IUserRepository',
            'App\Repositories\User\UserRepository'
        );
        $this->app->bind(
            'App\Repositories\Furlough\IFurloughRepository',
            'App\Repositories\Furlough\FurloughRepository'
        );
    }
}
