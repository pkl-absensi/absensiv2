<?php

namespace App\Utils\Messages;

class CRUDMessage
{
    const CREATE = 'Berhasil Menyimpan Data';
    const UPDATE = 'Berhasil Update Data';
    const DELETE = 'Berhasil Menghapus Data';

    const EMPTY = 'Data Tidak Ada';
}
