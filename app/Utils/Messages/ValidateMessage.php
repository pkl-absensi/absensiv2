<?php
namespace App\Utils\Messages;

class ValidateMessage {
    CONST TICKETCATIDREQUIRED = 'Ticketcategory_id Wajib Diisi';
    CONST NAMEREQUIRED = 'Name Wajib Diisi';
    CONST NAMEINVALID = 'Name Tidak Diketahui';
    CONST DESCREQUIRED = 'Description Wajib Diisi';
    CONST DESCMIN = 'Karakter Terlalu Pendek';
    CONST MINIMUMCHAR = 'Karakter Terlalu Pendek';
    CONST MAXIMUMCHAR = 'Karakter Terlalu Panjang';
    CONST STATUSREQUIRED = 'Status Wajib Diisi';
    CONST STATUSINVALID = 'Status Tidak Diketahui';
    CONST DEFAULTREQUIRED = 'Wajid Diisi';
    CONST RULEREQUIRED = 'Rule Wajib Diisi';
    CONST RULEINVALID = 'Rule Tidak Diketahui';
}
