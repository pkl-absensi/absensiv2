<?php
namespace App\Constants;

class VersionConst
{
    public static $apiV1 = '/api/v1/';
    public static $apiV2 = '/api/v2/';

    public const CREATED_AT = 'dateCreated';
    public const UPDATED_AT = 'dateUpdated';
    public const DELETED_AT = 'dateDeleted';
}