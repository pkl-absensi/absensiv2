<?php

namespace App\Repositories\Report;

interface IReportRepository
{
    public function getReportMonthlyEmployee($request, $date);
    public function getReportMonthlyByEmployeeId($request, $id, $date);
    public function getReportByEmployeeId($request, $id);
    public function getMyReportMonthly($request, $date);
    public function getMyReport($request);
    // public function createReportEmployeeMonthly();
}
