<?php

namespace App\Repositories\Report;

use App\Constants\PaginatorConst;
use App\Http\Resources\Attendances;
use App\Models\Attendance;
use App\Models\Furlough;
use App\Models\Report;
use App\Repositories\Report\IReportRepository;
use DateTime;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

class ReportRepository implements IReportRepository
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getReportMonthlyEmployee($request, $date)
    {
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $Month = date("m", strtotime(($date != null) ? $date : date('Y-m-d')));
        $Year = date("Y", strtotime(($date != null) ? $date : date('Y-m-d')));
        $reports = Report::whereMonth('date', $Month)->whereYear('date', $Year);
        $reports = $reports->paginate($limit);
        return $reports;
    }

    public function getReportMonthlyByEmployeeId($request, $id, $date = null)
    {
        $Month = date("m", strtotime(($date != null) ? $date : date('Y-m-d')));
        $Year = date("Y", strtotime(($date != null) ? $date : date('Y-m-d')));
        $report = Report::where('user_id', $id)->whereMonth('date', $Month)->whereYear('date', $Year)->first();
        if ($report == null) {
            return null;
        }
        return $report;
    }

    public function getReportByEmployeeId($request, $id)
    {
        $userId = $id;
        $attendance = Attendance::where('user_id', $userId)->get();
        $furlough = Furlough::where('user_id', $userId)->get();
        if ($attendance->isEmpty()) {
            return null;
        }
        $onTime = $attendance->where('checkInStatus', 'Tepat waktu')->count();
        $late = $attendance->where('checkInStatus', 'Telat')->count();
        $notPresent = $attendance->where('checkInStatus', 'Tidak hadir')->count();
        $permit = $attendance->where('checkInStatus', 'Izin')->count();
        $sick = $attendance->where('checkInStatus', 'Sakit')->count();
        $checkOutEarly = $attendance->where('checkOutStatus', 'Pulang awal')->count();
        $totalHours = $attendance->sum(function ($t) {
            $timeIn = DateTime::createFromFormat('Y-m-d H:i:s', ($t->timeIn != null) ? $t->timeIn : date('Y-m-d H:i:s'));
            $timeOut = DateTime::createFromFormat('Y-m-d H:i:s', ($t->timeOut != null) ? $t->timeOut : date('Y-m-d H:i:s'));
            $diff = $timeOut->diff($timeIn);
            return $diff->h;
        });
        $totalFurloughs = $furlough->sum('furloughTotal');
        $report = new Report;
        $report->user_id = $userId;
        $report->onTime = $onTime;
        $report->late = $late;
        $report->notPresent = $notPresent;
        $report->permit = $permit;
        $report->sick = $sick;
        $report->checkOutEarly = $checkOutEarly;
        $report->totalHoursWorked = $totalHours;
        $report->totalFurloughs = $totalFurloughs;
        return $report;
    }

    public function getMyReportMonthly($request, $date = null)
    {
        $Month = date("m", strtotime(($date != null) ? $date : date('Y-m-d')));
        $Year = date("Y", strtotime(($date != null) ? $date : date('Y-m-d')));
        $report = Report::where('user_id', Auth::user()->id)->whereMonth('date', $Month)->whereYear('date', $Year)->first();
        if ($report == null) {
            return null;
        }
        return $report;
    }
    public function getMyReport($request)
    {
        $userId = Auth::user()->id;
        $attendance = Attendance::where([['user_id', '=', $userId], ['dateDeleted', '=', null]])->get();
        $furlough = Furlough::where('user_id', $userId)->get();
        if ($attendance->isEmpty()) {
            return null;
        }
        $onTime = $attendance->where('checkInStatus', 'Tepat waktu')->count();
        $late = $attendance->where('checkInStatus', 'Telat')->count();
        $notPresent = $attendance->where('checkInStatus', 'Tidak hadir')->count();
        $permit = $attendance->where('checkInStatus', 'Izin')->count();
        $sick = $attendance->where('checkInStatus', 'Sakit')->count();
        $checkOutEarly = $attendance->where('checkOutStatus', 'Pulang awal')->count();
        $totalHours = $attendance->sum(function ($t) {
            $timeIn = DateTime::createFromFormat('Y-m-d H:i:s', ($t->timeIn != null) ? $t->timeIn : date('Y-m-d H:i:s'));
            $timeOut = DateTime::createFromFormat('Y-m-d H:i:s', ($t->timeOut != null) ? $t->timeOut : date('Y-m-d H:i:s'));
            $diff = $timeOut->diff($timeIn);
            return $diff->h;
        });
        $totalFurloughs = $furlough->sum('furloughTotal');
        $report = new Report;
        $report->user_id = $userId;
        $report->onTime = $onTime;
        $report->late = $late;
        $report->notPresent = $notPresent;
        $report->permit = $permit;
        $report->sick = $sick;
        $report->checkOutEarly = $checkOutEarly;
        $report->totalHoursWorked = $totalHours;
        $report->totalFurloughs = $totalFurloughs;

        return $report;
    }
}
