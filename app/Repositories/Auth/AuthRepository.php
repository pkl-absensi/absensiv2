<?php

namespace App\Repositories\Auth;

use App\Repositories\Auth\IAuthRepository;
use Illuminate\Support\Facades\Auth;


class AuthRepository implements IAuthRepository
{
    public function login($request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        $user = Auth::user()->id;

        // $token = Auth::claims(['employee_id' => $user])->attempt($credentials);

        return $token;
    }

    public function refreshToken($request)
    {
        $newToken = auth::refresh();
        return $newToken;
    }

    public function logOut($request)
    {
        $logout = auth::logout();
        return $logout;
    }
}
