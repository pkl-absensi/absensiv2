<?php

namespace App\Repositories\Auth;

interface IAuthRepository
{
    public function login($request);
    public function refreshToken($request);
    public function logOut($request);
}
