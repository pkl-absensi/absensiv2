<?php

namespace App\Repositories\Furlough;

use App\Http\Resources\Attendances;
// use App\Models\Attendance;
use App\Models\Furlough;
use App\Models\User;
use App\Repositories\Furlough\IFurloughRepository;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Constants\PaginatorConst;
use Illuminate\Pagination\Paginator;
use App\Services\Keyword;
use App\Helpers\DateHelper;

class FurloughRepository implements IFurloughRepository
{
    public function addFurlough($request)
    {
        $user = User::find(isset($request->id) ? $request->id : Auth::user()->id);
        $dt = new DateHelper();
        $user->furloughStart = $request->furloughStart;
        $user->furloughEnd = $request->furloughEnd;
        $totalCuti = $dt->diffDate($request->furloughStart, $request->furloughEnd);
        $test1 = $user->furloughRemaining;
        $result2 = $test1 - $totalCuti;
        //Mengurangi Jatah Cuti
        if ($result2 >= 0) {
            $user->furloughRemaining = $result2;
            $user->save();
            //menyimpan ke database
            $user->refresh();

            //Menyimpan ke database di table cuti
            $furlough = new Furlough();
            $furlough->user_id = Auth::user()->id;
            $furlough->furloughStart = $request->furloughStart;
            $furlough->furloughEnd = $request->furloughEnd;
            $furlough->furloughTotal = $totalCuti;
            $furlough->save();
            return $furlough;
        }
        return null;
    }

    public function getFurloughById($request, $user_id)
    {
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $defaults =  Furlough::where("user_id", $user_id);
        $defaults = Keyword::search($defaults, $request);
        $defaults = Keyword::order($defaults, $request);
        $furlough = $defaults->paginate($limit);
        Keyword::paginateUrl($furlough, $request, 'furlough');
        return $furlough;
    }

    public function getSelfFurlough($request)
    {
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $defaults =  Furlough::where("user_id", Auth::user()->id);
        $defaults = Keyword::search($defaults, $request);
        $defaults = Keyword::order($defaults, $request);
        $furlough = $defaults->paginate($limit);
        Keyword::paginateUrl($furlough, $request, 'furlough');
        return $furlough;
    }
}
