<?php

namespace App\Repositories\Furlough;

interface IFurloughRepository
{
    public function addFurlough($request);
    public function getSelfFurlough($request);
    public function getFurloughById($request, $user_id);
}
