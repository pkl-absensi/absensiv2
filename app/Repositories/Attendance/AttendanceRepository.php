<?php

namespace App\Repositories\Attendance;

use App\Constants\PaginatorConst;
use App\Models\Attendance;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Attendance\IAttendanceRepository;
use DateTime;
use Illuminate\Pagination\Paginator;
use Illuminate\Validation\ValidationException;
use Grei\TanggalMerah;

class AttendanceRepository implements IAttendanceRepository
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function checkInPresence($request)
    {
        $userId = Auth::user()->id;
        $presence = new Attendance;
        $date = new TanggalMerah();
        if($date->check()) {
            return null;
        }
        if ($presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first() == null) {
            $timeNow = date("Y-m-d H:i:s");
            $morning = strtotime('09:00:00');
            $presence->user_id = $userId;
            $presence->timeIn = $timeNow;
            $presence->date = date('Y-m-d');
            $presence->checkInStatus = (strtotime($timeNow) < $morning) ? "Tepat waktu" : "Telat";
            $presence->save();
            $presence = $presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first();
            return $presence;
        }
        return null;
    }

    public function checkOutForBreakPresence($request)
    {
        $userId = Auth::user()->id;
        $presence = new Attendance;
        $date = new TanggalMerah();
        if($date->check()) {
            return null;
        }
        $presence = $presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first();
        if ($presence == null) {
            return null;
        }
        if ($presence->breakTimeOut == null) {
            $timeNow = date("Y-m-d H:i:s");
            $presence->breakTimeOut = $timeNow;
            $presence->save();
            $presence = $presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first();
            return $presence;
        }
        return null;
    }

    public function checkInAfterBreakPresence($request)
    {
        $userId = Auth::user()->id;
        $presence = new Attendance;
        $date = new TanggalMerah();
        if($date->check()) {
            return null;
        }
        $presence = $presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first();
        if ($presence == null) {
            return null;
        }
        if ($presence->breakTimeOut != null && $presence->breakTimeIn == null) {
            $timeNow = date("Y-m-d H:i:s");
            $presence->breakTimeIn = $timeNow;
            $presence->save();
            $presence = $presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first();
            return $presence;
        }
        return null;
    }

    public function checkOutPresence($request)
    {
        $userId = Auth::user()->id;
        $presence = new Attendance;
        $date = new TanggalMerah();
        if($date->check()) {
            return null;
        }
        $presence = $presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first();
        if ($presence == null) {
            return null;
        }
        if ($presence->breakTimeOut != null && $presence->breakTimeIn != null && $presence->timeOut == null) {
            $timeNow = date("Y-m-d H:i:s");
            $presence->timeOut = $timeNow;
            if (strtotime($timeNow) <= strtotime('18:00:00') && strtotime($timeNow) >= strtotime('17:00:00')) {
                $presence->checkOutStatus = 'Tepat waktu';
            } else if (strtotime($timeNow) > strtotime('18:00:00')) {
                $presence->checkOutStatus = 'Lembur';
            } else if (strtotime($timeNow) < strtotime('17:00:00')) {
                $presence->checkOutStatus = 'Pulang awal';
            } else {
                $presence->checkOutStatus = 'Tepat waktu';
            }
            $presence->save();
            $presence = $presence->where([['date', '=', date('Y-m-d')], ['user_id', '=', $userId]])->first();
            return $presence;
        }
        return null;
    }

    public function attendance($request)
    {
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $employees = User::where('status','!=','Retired');
        $employees = $employees->paginate($limit);
        return $employees;
    }

    public function attendanceSelf($request)
    {
        $employee = User::where([['status','!=','Retired'], ['id', '=', Auth::user()->id]])->first();
        return $employee;
    }

    public function editStatusAttendance($request, $id)
    {
        $date = new DateTime($request->date);
        $now = new DateTime();
        $attendance = Attendance::where([
            ['user_id', '=', $id],
            ['date', '=', $request->date]
        ])->first();
        if ($attendance == null) {
            $attendance = new Attendance();
            $attendance->user_id = $id;
            $attendance->date = date('Y-m-d');
        }
        $diff = $date->diff($now);
        if (abs($diff->d) >= 3) {
            return null;
        }
        $attendance->checkInStatus = ($request->status) ? $request->status : $attendance->checkInStatus;
        $attendance->save();
        return $attendance;
    }
}
