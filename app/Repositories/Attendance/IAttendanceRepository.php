<?php

namespace App\Repositories\Attendance;

interface IAttendanceRepository
{
    public function checkInPresence($request);
    public function checkOutForBreakPresence($request);
    public function checkInAfterBreakPresence($request);
    public function checkOutPresence($request);
    public function attendance($request);
    public function attendanceSelf($request);
    public function editStatusAttendance($request, $id);
}
