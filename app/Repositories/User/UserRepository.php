<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Models\Furlough;
use Illuminate\Support\Facades\Auth;
use App\Repositories\User\IUserRepository;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Hash;
use App\Constants\PaginatorConst;
use Illuminate\Pagination\Paginator;
use App\Services\Keyword;
use App\Services\Rule;

class UserRepository implements IUserRepository
{

    public function getActiveUser($request)
    {
        $status = "Active";
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $defaults =  User::where("status", $status);
        $defaults = Keyword::search($defaults, $request);
        $defaults = Keyword::order($defaults, $request);
        $users = $defaults->paginate($limit);
        Keyword::paginateUrl($users, $request, 'users');
        return $users;
    }

    public function getRetiredUser($request)
    {
        $status = "Retired";
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $defaults =  User::where("status", $status);
        $defaults = Keyword::search($defaults, $request);
        $defaults = Keyword::order($defaults, $request);
        $users = $defaults->paginate($limit);
        Keyword::paginateUrl($users, $request, 'users');
        return $users;
    }

    public function getFurloughUser($request)
    {
        $status = "Furlough";
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $defaults =  User::where("status", $status);
        $defaults = Keyword::search($defaults, $request);
        $defaults = Keyword::order($defaults, $request);
        $users = $defaults->paginate($limit);
        Keyword::paginateUrl($users, $request, 'users');
        return $users;
    }

    public function getAll($request)
    {
        $limit = $request->get('limit', PaginatorConst::$Size);
        $page = $request->get('page', PaginatorConst::$Page);
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $defaults = new User;
        $defaults = Keyword::search($defaults, $request);
        $defaults = Keyword::order($defaults, $request);
        $users = $defaults->paginate($limit);
        Keyword::paginateUrl($users, $request, 'users');
        return $users;
    }
    public function getUserById($request, $userId)
    {
        $user = User::find($userId);
        return $user;
    }

    public function getSelf($request)
    {
        $user = User::find(Auth::user()->id);
        return $user;
    }

    public function registerUser($request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = isset($request->password) ? Hash::make($request->password) : null;
        $user->address = $request->address;
        $user->birthDate = $request->birthDay;
        $user->phoneNumber = $request->phoneNumber;
        $user->position = $request->position;
        $user->joinDate = date("Y-m-d");
        // $user->endDate = $request->endDate;
        $user->status = $request->status;
        $user->role = $request->role;
        $user->furloughRemaining = 12;
        $user->save();
        $user->refresh();


        return $user;
    }

    public function updateUser($request, $userId)
    {
        $user = User::find($userId);
        $user->name = isset($request->name) ? $request->name : $user->name;
        $user->email = isset($request->email) ? $request->email : $user->email;
        $user->password = isset($request->password) ? Hash::make($request->password) : $user->password;
        $user->address = isset($request->address) ? $request->address : $user->address;
        $user->birthDate = isset($request->birthDay) ? $request->birthDay : $user->birthDate;
        $user->phoneNumber = isset($request->phoneNumber) ? $request->phoneNumber : $user->phoneNumber;
        $user->position = isset($request->position) ? $request->position : $user->position;
        // $user->joinDate = $request->joinDate;
        // $user->endDate = $request->endDate;
        // $user->status = $request->status;
        $user->save();

        return $user;
    }

    public function updateUserSelf($request)
    {
        $user = User::find(Auth::user()->id);
        $user->name = isset($request->name) ? $request->name : $user->name;
        $user->email = isset($request->email) ? $request->email : $user->email;
        if (isset($request->password) && isset($request->oldpassword)) {
            if (Hash::check($request->oldpassword, $user->password)) {
                $user->password = Hash::make($request->password);
            } else {
                return null;
            }
        }
        $user->address = isset($request->address) ? $request->address : $user->address;
        $user->birthDate = isset($request->birthDate)  ? $request->birthDate : $user->birthDate;
        $user->phoneNumber = isset($request->phoneNumber) ? $request->phoneNumber : $user->phoneNumber;
        // $user->position = $request->position;
        // $user->joinDate = $request->joinDate;
        // $user->endDate = $request->endDate;
        // $user->status = $request->status;
        $user->save();

        return $user;
    }

    public function updateRetiredUser($request, $userId)
    {
        $user = User::find($userId);
        $user->endDate = date("Y-m-d H:i:s");
        $user->status = "Retired";
        $user->save();
        return $user;
    }

    public function refreshStatus($request)
    {
        $status = "Furlough";

        $user = User::where("status", $status)->get();
        $i = 1;
        foreach ($user as $key) {
            $nowDate = strtotime(date("Y-m-d"));
            $furloughStart = strtotime($key->furloughStart);
            $furloughEnd = strtotime($key->furloughEnd);
            if ($nowDate > $furloughStart && $nowDate < $furloughEnd) {
                $key->status = "Furlough";
                $key->save();
            } else {
                $key->status = "Active";
                $key->furloughStart = null;
                $key->furloughEnd = null;
                $key->save();
            }
            $i = $i + 1;
        }
        return null;
    }
}
