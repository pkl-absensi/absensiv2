<?php

namespace App\Repositories\User;

interface IUserRepository
{
    public function getAll($request);
    public function getUserById($request, $userId);
    public function getActiveUser($request);
    public function getRetiredUser($request);
    public function getFurloughUser($request);
    public function registerUser($request);
    public function updateUser($request, $userId);
    public function updateUserSelf($request);
    public function updateRetiredUser($request, $userId);
    public function getSelf($request);
    public function refreshStatus($request);
}
